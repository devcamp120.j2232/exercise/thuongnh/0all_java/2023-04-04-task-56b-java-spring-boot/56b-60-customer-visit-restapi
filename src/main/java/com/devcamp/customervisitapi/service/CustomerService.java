package com.devcamp.customervisitapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customervisitapi.model.Customer;

@Service
public class CustomerService {
    

    // khởi tạo các đối tượng khách hàng 
    Customer customer1 = new Customer("thuong");
    Customer customer2 = new Customer("duong");
    Customer customer3 = new Customer("huy");
    Customer customer4 = new Customer("phuong");
    // phương thức trả về Arr là các khách hàng 
    public ArrayList <Customer>  getCustomerList() {
        ArrayList <Customer>  allCustomer = new ArrayList<Customer>();
        allCustomer.add(customer1);
        allCustomer.add(customer2);
        allCustomer.add(customer3);
        allCustomer.add(customer4);
        return allCustomer;
    }
    

}
